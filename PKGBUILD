# Maintainer : Adrien Sohier <adrien.sohier@art-software.fr>

pkgbase=bashtools-git
pkgver=1.2
pkgrel=1
pkgdesc="Some useful bash tools"

pkgname=('vpn-ssh' 'trash' 'sch' 'zicsort' 'runrss' 'msm' 'stowmir')

_gitrepo="http://git.art-software.fr/git"
_gitname="bashtools"

arch=('any')
url="http://www.art-software.fr"
license=(GPLv3)

source=("git+${_gitrepo}/${_gitname}.git")
md5sums=(SKIP)

depends=('bash')
makedepends=('git')

package_vpn-ssh()
{
	pkgdesc="A simple SSH-based VPN tunnel daemon. Needs root connection to distant server"
	backup=('etc/vpn.cfg')
	depends=('bash' 'iputils' 'iproute2' 'net-tools' 'openssh' 'iptables')
	pkgver=1.2.1

	cd "${srcdir}/${_gitname}"
	git checkout vpn-ssh

	install -dm755 ${pkgdir}/usr/bin
	install -dm755 ${pkgdir}/usr/lib/systemd/system
	install -dm755 ${pkgdir}/etc

	install -m755 vpn ${pkgdir}/usr/bin/
	install -m755 vpn_forwd.sh ${pkgdir}/usr/bin/vpn_install
	install -m644 vpn.cfg ${pkgdir}/etc/
	install -m644 vpn.service ${pkgdir}/usr/lib/systemd/system/
}
package_stowmir()
{
	pkgdesc="A script to manage your stow repositories mirrors"
	depends=('bash' 'rsync' 'stow')
	pkgver=1.2

	cd "${srcdir}/${_gitname}"
	git checkout stowmir

	install -Dm755 mir.sh ${pkgdir}/stow
}
package_trash()
{
	pkgver=1.1
	pkgdesc="A simple trash manager in console"

	cd "${srcdir}/${_gitname}"
	git checkout trash

	install -dm755 ${pkgdir}/usr/bin
	install -m755 trash ${pkgdir}/usr/bin/
}
package_sch()
{
	pkgdesc="A synchronization manager using RSync"
	depends=('bash' 'openssh' 'rsync')
	pkgver=1.1

	cd "${srcdir}/${_gitname}"
	git checkout sch

	install -dm755 ${pkgdir}/usr/bin
	install -m755 sch ${pkgdir}/usr/bin/
}
package_zicsort()
{
	pkgdesc="A music playlist manager"
	depends=('bash' 'mplayer')
	pkgver=1.1

	cd "${srcdir}/${_gitname}"
	git checkout zicsort

	install -dm755 ${pkgdir}/usr/bin
	install -dm755 ${pkgdir}/usr/lib/zicsort

	sed -i '/^LIBPATH=/s@^.*$@LIBPATH=/usr/lib/zicsort@' zicsort

	install -m755 zicsort ${pkgdir}/usr/bin/
	install -m755 lib/*.sh ${pkgdir}/usr/lib/zicsort/

	git checkout -- zicsort
}
package_runrss()
{
	pkgdesc="Open an url with various services"
	depends=('bash' 'kdebase-kdialog')
	pkgver=1.0

	cd "${srcdir}/${_gitname}"
	git checkout runrss

	install -dm755 ${pkgdir}/usr/bin

	install -m755 runrss ${pkgdir}/usr/bin/
}
package_msm()
{
	pkgdesc="Minecraft Server Monitor"
	depends=('bash' 'java-runtime')
	backup=('etc/msm.cfg')
	pkgver=1.0

	cd "${srcdir}/${_gitname}"
	git checkout msm

	install -dm755 ${pkgdir}/usr/bin
	install -dm755 ${pkgdir}/etc

	install -m755 msm ${pkgdir}/usr/bin/
	install -m644 msm.cfg ${pkgdir}/etc/
}
